package simpleLinkedList;

public class SingleLinedList implements SimpleList {
        private SingleLinedNode head = null;
        private boolean isEmpty(){
            return head==null;
        }


        @Override
        public int size() {
            if (isEmpty()) {
                return 0;
            }
            SingleLinedNode current = head;
            int size =1;
            while (current.hasNext()) {
                current=current.next();
                size++;
            }
            return size;
        }
        @Override
        public void add(int element) {
            if (isEmpty()){
                SingleLinedNode node = new SingleLinedNode(element,null);
                head=node;
            }else {
                SingleLinedNode node = new SingleLinedNode(element,head);
                head=node;
            }

        }
        @Override
        public void addLast(int element) {
            if (isEmpty()){
                add(element);
            }else {
                SingleLinedNode current = head;
                while(current.hasNext()){
                    current=current.next();
                }
                SingleLinedNode node = new SingleLinedNode(element,null);
                current.setNext(node);
            }
        }
        @Override
        public void add(int element, int index) {
            if (index>size()) {
                throw new IllegalArgumentException();
            }
            if (index==0){
                add(element);
            }else {
                int nodeToSkip = index-1;
                SingleLinedNode current = head;
                while (nodeToSkip>0){
                    current = current.next();
                    nodeToSkip--;
                }
                SingleLinedNode newMode = new SingleLinedNode(element,current.next());
                current.setNext(newMode);
            }
        }
        @Override
        public int removeFirst() {
            if (isEmpty()){
                throw new IllegalArgumentException();
            }
            int resoult = head.value();
            head = head.next();
            return resoult;
        }
        @Override
        public int removeLast() {
            if (isEmpty()) {
                throw new IllegalArgumentException();
            }
            SingleLinedNode current = head;
            SingleLinedNode nextNode = head.next();
            if (nextNode == null) {
                return removeFirst();
            }else {
                while (nextNode.hasNext()){
                    current = nextNode;
                    nextNode=nextNode.next();
                }
                current.setNext(null);
                return nextNode.value();

            }

        }


        @Override
        public int remove(int idx) {
            if (idx == 0) {
                return removeFirst();
            }
            if (isEmpty()) {
                throw new IllegalArgumentException();
            }
            SingleLinedNode current = head;
            int nodedToSkip = idx - 1;
            while (current.hasNext() && nodedToSkip > 0) {
                current = current.next();
                nodedToSkip--;
            }
            if (nodedToSkip == 0 && current.next() != null) {
                int score = current.next().value();
                current.setNext(current.next().next());
                return score;
            }
            throw new IllegalArgumentException();
        }

        @Override
        public int removeElement(int element) {
            if (isEmpty()){
                throw new IllegalArgumentException();
            }
            if (head.value()==element){
                return removeFirst();
            }
            SingleLinedNode current = head;
            while (current.hasNext()){
                if (current.next().value()==element){
                    int result = current.next().value();
                    current.setNext(current.next().next());
                    return result;
                }else {
                    current=current.next();
                }

            }
            throw new IllegalArgumentException();
        }
        @Override
        public boolean contains(int element) {
            if (isEmpty()){
                return false;
            }
            SingleLinedNode node =head;
            while(node.hasNext()|| node.value()==element){
                if (node.value()==element){
                    return true;
                }else {
                    node=node.next();
                }
            }
            return false;
        }

        @Override
        public int get(int idx) {
            if (idx >= size()){
                throw new IllegalArgumentException();}

            SingleLinedNode current = head;
            int nodeToSkip=idx;
            while (nodeToSkip>0){
                current=current.next();
                nodeToSkip--;
            }
            return current.value();


        }
    }
