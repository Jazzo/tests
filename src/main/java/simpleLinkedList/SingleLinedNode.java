package simpleLinkedList;

public class SingleLinedNode {
    public int value() {
        return value;
    }

    public SingleLinedNode next() {
        return next;
    }

    private int value;

    public void setNext(SingleLinedNode next) {
        this.next = next;
    }

    private SingleLinedNode next;

    SingleLinedNode(int value, SingleLinedNode next) {
        this.value = value;
        this.next = next;
    }

    public boolean hasNext() {
        return next != null;
    }

}
