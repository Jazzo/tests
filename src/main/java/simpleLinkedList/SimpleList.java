package simpleLinkedList;

public interface SimpleList {
    int size();
    void add(int element);
    void addLast(int element);
    void add(int element,int index);
    int removeFirst();
    int removeLast();
    int remove(int idx);
    int removeElement(int element);
    boolean contains(int element);
    int get(int idx);
}
