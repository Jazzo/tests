package onlineShop;

import java.util.Comparator;
import java.util.Objects;

public class Item implements Comparable<Item>{
    private String name;
    private double price;

    public Item(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Item item = (Item) o;
        return name.equals(item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price);
    }

    @Override
    public int compareTo(Item o) {
        if (o == null) {
            return 1;
        }
        int comparision = this.getName().compareTo(o.getName());
        if (comparision != 0) {
            return comparision;
        }
        return Double.compare(this.getPrice(), o.getPrice());
    }
}
