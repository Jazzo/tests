package onlineShop;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Basket {
    private final Map<Item, Integer> order = new HashMap<Item, Integer>();

    public void addItem(Item item) {
        order.put(item, 1);
    }

    public void addMoreItem(Item item, int quantity) {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Nie właściwa ilość");
        }
        if (order.containsKey(item)) {
            quantity = order.get(item) + quantity;
        }
        order.put(item, quantity);
    }
    public void remove(Item item) {
        remove(item, 1);
    }
    public void remove(Item item, int quantity) {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Nie właściwa ilość");
        }
        quantity = order.get(item) - quantity;
        if (quantity == 0) {
            order.remove(item);
        } else if (quantity < 0)
            throw new IllegalStateException("Nie posiadasz w koszyku tyle przedmiotów");
        else {
            order.put(item, quantity);
        }
    }
    public double getOrderValue(){
        double orderValue =0;
        for (Map.Entry<Item,Integer> itemOrder:order.entrySet()){
            orderValue+=itemOrder.getKey().getPrice()*itemOrder.getValue();
        }
        return orderValue;
    }
    public Map<Item,Integer>getOrder(){
        return Collections.unmodifiableMap(order);
    }

}
