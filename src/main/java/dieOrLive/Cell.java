package dieOrLive;

public class Cell implements Cloneable {
    private boolean alive;

    public Cell(boolean alive) {
        this.alive = alive;
    }
    public boolean isAlive() {
        return alive;
    }
    public void setAlive(boolean alive) {
        this.alive = alive;
    }
    public Cell clone(){
        return new Cell(alive);
    }
    public void changeState(int neighboursCount){
        if (alive){
            if (neighboursCount<2){
                alive=false;
            }else if (neighboursCount>3){
                alive=false;
            }
        }else {
            if (neighboursCount==3){
                alive=true;
            }
        }
    }
}
