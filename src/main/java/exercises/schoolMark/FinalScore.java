package exercises.schoolMark;

public class FinalScore {
    Pe pe = new Pe();
    Math math = new Math();

    public int finalStudentScore() {
        int value = 0;
        value = pe.sumaPeMarks() + math.sumaMathMarks();
        value = value / 2;
        return value;
    }

    public static void main(String[] args) {
        FinalScore finalScore = new FinalScore();
        System.out.println(finalScore.finalStudentScore());
    }

}
