package exercises;

public class ArrayAndLoopEx {
    private Integer[][] board;

    public ArrayAndLoopEx(Integer[][] board) {
        this.board = board;
    }

    public Integer[][] getBoard() {
        return board;
    }

    public void setBoard(Integer[][] board) {
        this.board = board;
    }

    public static ArrayAndLoopEx sudoku() {
        Integer[][] board = new Integer[][]{
                new Integer[]{2, null, null, 6, null, 7, 5, null, null},
                new Integer[]{null, null, null, null, null, null, null, 9, 6},
                new Integer[]{6, null, 7, null, null, 1, 3, null, null},

                new Integer[]{null, 5, null, 7, 3, 2, null, null, null},
                new Integer[]{null, 7, null, null, null, null, null, 2, null},
                new Integer[]{null, null, null, 1, 8, 9, null, 7, null},

                new Integer[]{null, null, 3, 5, null, null, 6, null, 4},
                new Integer[]{8, 4, null, null, null, null, null, null, null},
                new Integer[]{null, null, 5, 2, null, 6, null, null, 8}
        };
        return new ArrayAndLoopEx(board);
    }


    public static void exWithFor() {
        for (int i = 20; i >= 10; i--) {
            System.out.println(i);
        }
    }

    public static void exWithWhile() {
        int number = 20;
        while (number > 10) {
            System.out.println(number);
            number--;
        }
    }

    public static void exWithForAndOddNumber() {
        for (int i = -10; i <= 40; i++) {
            if (i % 2 == 0) {
                continue;
            } else {
                System.out.println(i);
            }
        }
    }

    public static void exWithWhileAndOddNumber() {
        int number = -10;
        while (number < 40) {
            if (number % 2 == 0) {
                number++;
                continue;
            } else
                System.out.println(number);
            number++;
        }
    }

    public static int sumArray(int[] numbers) {
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        return sum;
    }

    public static int chooseMostValueNumber(int[] numbers) {
        int bigger = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > bigger) {
                bigger = numbers[i];
            }
        }
        return bigger;
    }


}
