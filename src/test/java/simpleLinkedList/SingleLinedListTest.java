package simpleLinkedList;

import org.junit.Test;

import static org.junit.Assert.*;

public class SingleLinedListTest {
    private SingleLinedList underTest = new SingleLinedList();

    @Test
    public void test(){
        underTest.add(1);
        assertEquals(1,underTest.size());
        assertEquals(1,underTest.get(0));
    }
    @Test
    public void add_secondAddedElementIsFirstOnList(){
        underTest.add(10);
        underTest.add(15);
        assertEquals(2,underTest.size());
        assertEquals(10,underTest.get(1));
        assertEquals(15,underTest.get(0));
    }
    @Test
    public void add_insertion(){
        underTest.add(10);
        underTest.add(15);
        underTest.add(25);
        underTest.add(50,2);

        assertEquals(50,underTest.get(2));
        assertEquals(10,underTest.get(3));
    }
    @Test
    public void remove_firstElement(){
        underTest.add(10);
        underTest.add(15);
        underTest.add(25);

        assertEquals(3,underTest.size());
        assertEquals(25,underTest.removeFirst());
        assertEquals(15,underTest.get(0));
        assertEquals(2,underTest.size());
    }
    @Test
    public void remove_lastElement(){
        underTest.add(100);
        underTest.add(150);
        underTest.add(250);

        assertEquals(100,underTest.removeLast());
    }

    @Test
    public void removes_removesElementFromIndex(){
        underTest.add(100);
        underTest.add(150);
        underTest.add(250);
        assertEquals(100,underTest.remove(2));
        assertEquals(2,underTest.size());
    }


}