package onlineShop;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class BasketTest {

    private static final double PRICE =1;

    private Basket basket;
    private Item autko;

    @Before
    public void setUp() {
        autko = new Item("autko", 40);
        basket = new Basket();
    }

    private static Map<Item, Integer> createOrder(Object... mapContent) {
        Map<Item, Integer> result = new HashMap<>();
        for (int index = 0; index < mapContent.length; index += 2) {
            Item item = (Item) mapContent[index];
            Integer quantity = (Integer) mapContent[index + 1];
            result.put(item, quantity);
        }
        return result;
    }

    @Test
    public void shouldAllowToAddItemToBasket() {
        basket.addItem(autko);
        Map<Item, Integer> expected = createOrder(autko, 1);
        assertEquals(expected, basket.getOrder());
    }

    @Test
    public void shouldAllowToAddTheSameItemTwice() {
        basket.addItem(autko);
        basket.addItem(autko);

        Map<Item, Integer> expected = createOrder(autko, 2);

        assertEquals(expected, basket.getOrder());

    }

    @Test
    public void shouldAllowToAddItemWithQuantityOne() {
        basket.addMoreItem(autko, 1);

        Map<Item, Integer> expected = createOrder(autko, 1);

        assertEquals(expected, basket.getOrder());
    }
    @Test
    public void shouldAllowToAddItemWithQuantityMany() {
        basket.addMoreItem(autko, 34);

        Map<Item, Integer> expected = createOrder(autko, 34);

        assertEquals(expected, basket.getOrder());
    }
    @Test(expected = IllegalArgumentException.class)
    public void shouldntAllowToAddItemWithQuantityZero() {
        basket.addMoreItem(autko, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldntAllowToAddItemWithNegativeQuantity() {
        basket.addMoreItem(autko, -10);
    }
    @Test
    public void shouldAllowToRemoveItemFromBasket() {
        basket.addMoreItem(autko, 2);
        basket.remove(autko);

        Map<Item, Integer> expected = createOrder(autko, 1);

        assertEquals(expected, basket.getOrder());
    }

    @Test
    public void shouldRemoveAllItemsFromBasket() {
        basket.addItem(autko);
        basket.remove(autko);

        Map<Item, Integer> expected = Collections.emptyMap();

        assertEquals(expected, basket.getOrder());
    }

    @Test
    public void shouldAllowToRemove2ItemsAtOnce() {
        basket.addMoreItem(autko, 3);
        basket.remove(autko, 2);

        Map<Item, Integer> expected = createOrder(autko, 1);

        assertEquals(expected, basket.getOrder());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldTrowExcptionWhenRemoving0Items() {
        basket.remove(autko, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldTrowExcptionWhenRemovingNegativeQuantity() {
        basket.remove(autko, -10);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenThereIsNoThatManyitemsToRemove() {
        basket.addMoreItem(autko, 1);
        basket.remove(autko, 2);
    }

    @Test
    public void shouldComputeSimpleOrderValue() {
        basket.addMoreItem(autko, 3);

        double expectedValue = autko.getPrice() * 3;

        assertEquals(expectedValue, basket.getOrderValue(),PRICE);
    }
}
