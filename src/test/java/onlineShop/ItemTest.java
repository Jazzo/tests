package onlineShop;

import org.junit.Test;

import static org.junit.Assert.*;

public class ItemTest {

    @Test
    public void twoItemsWIthTheSamePriceAndNameShouldBeEqual(){
        assertEquals(new Item("item",111),new Item("item",111));
    }
    @Test
    public void itemsWithDifferentNamesArentEaual(){
        assertNotEquals(new Item("itemik",1234L),new Item("item",1234));
    }
    @Test
    public void itemsWithTheSameNameShouldHaveTheSameHashCode(){
        assertEquals(new Item("item",111).hashCode(),new Item("item",111).hashCode());
    }
    @Test
    public void itemsWithDifferentShouldHaveDifferentHashCode(){
        assertNotEquals(new Item("item",111).hashCode(),new Item("items",111).hashCode());
    }
    @Test
    public void itemsWithDifferentPriceAndTheSameNameShouldBeOrdered(){
        assertEquals(-1,new Item("item1",0.1).compareTo(new Item("item1",0.2)));
    }

}